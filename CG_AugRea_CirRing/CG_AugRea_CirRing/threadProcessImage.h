//#pragma once
//#include "patterndetector.h"
//#include <opencv2\opencv.hpp>
//#include <opencv2/calib3d/calib3d.hpp>
//
//using namespace cv;
//using namespace std;
//
//struct strucRotTrasVec {
//	bool frameValid;
//	Mat rMat3x3;
//	Mat tvec;
//};
//
//struct CameraParams {
//	int numFrames,
//		imgWidth, imgHeight,
//		numCols, numRows;
//	float distanceKeypoints;
//	Mat cameraMatrix;
//	Mat distCoeffs;
//	bool isLoaded;
//};
//
//CameraParams readCameraParameters(string path)
//{
//	CameraParams params;
//	params.isLoaded = false;
//	FileStorage fs(path, FileStorage::READ);
//	if (!fs.isOpened()) {
//		cout << "Archivo XML no encontrado\n";
//		return params;
//	}
//	
//	fs["num_of_frames"] >> params.numFrames;
//	fs["image_width"] >> params.imgWidth;
//	fs["image_height"] >> params.imgHeight;
//	fs["num_cols"] >> params.numCols;
//	fs["num_rows"] >> params.numRows;
//	fs["distance_centers"] >> params.distanceKeypoints;
//	fs["Camera_Matrix"] >> params.cameraMatrix;
//	fs["Distortion_Coefficients"] >> params.distCoeffs;
//	fs.release();
//	params.isLoaded = true;
//	return params;
//}
//
//vector<Point3f> calcDistanceInWorldThreadRing(CameraParams camParams)
//{
//	vector<Point3f> realCenters;
//	float distanceKeyPoints = camParams.distanceKeypoints;
//	for (size_t i = 0; i < camParams.numCols; ++i)
//		for (size_t j = 0; j < camParams.numRows; ++j)
//			realCenters.push_back(Point3f(float(i * distanceKeyPoints), float(j * distanceKeyPoints), 0));
//	return realCenters;
//}
//
//strucRotTrasVec getRotTrasByFrameRing(Mat frame, CameraParams camParams, PatternDetector *detector, int idFrame) {
//	vector<Point2f> keypoints;
//	strucRotTrasVec res;
//	Mat rvec, tvec, rMat3x3;
//
//	res.frameValid = false;
//	detector->setImage(frame);
//
//	bool trackCorrect = detector->processingRingsPattern(keypoints, idFrame);
//	if (keypoints.size() != camParams.numCols * camParams.numRows) {
//		return res;
//	}
//	// generacion de los puntos reales
//	vector<Point3f> objectsPoints = calcDistanceInWorldThreadRing(camParams);
//	if (solvePnP(objectsPoints, keypoints, camParams.cameraMatrix, camParams.distCoeffs, rvec, tvec)) {
//		Rodrigues(rvec, rMat3x3);
//		res.frameValid = true;
//		res.tvec = tvec;
//		res.rMat3x3 = rMat3x3;
//	}
//	frame.release();
//	return res;
//}
