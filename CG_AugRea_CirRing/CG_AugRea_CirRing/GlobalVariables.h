#ifndef GLOBAL_VARIABLES_H
#define GLOBAL_VARIABLES_H

#include <vector>
#include <glm\glm.hpp>
#include <GL\glew.h>

extern glm::mat4 Projection;
extern glm::mat4 View;
extern glm::mat4 Model;

struct ShaderInfo {
	GLuint shaderID;
	std::vector<GLuint> uniformInfo;
};

#endif