#version 330 core

// Input vertex data, different for all executions of this shader.
layout(location = 0) in vec3 vertexPosition;
layout(location = 1) in vec2 vertexUV;
layout(location = 2) in vec3 vertexNormal;

// Indices of refraction
const float Air = 1.0;
const float Glass = 1.51714;

// Air to glass ratio of the indices of refraction (Eta)
const float Eta = Air / Glass;
 
// see http://en.wikipedia.org/wiki/Refractive_index Reflectivity
const float R0 = ((Air - Glass) * (Air - Glass)) / ((Air + Glass) * (Air + Glass));

// Output data ; will be interpolated for each fragment.
out vec2 UV;
out vec3 v_reflection;
out vec3 v_refraction;
out float v_fresnel;

// Values that stay constant for the whole mesh.
uniform mat4 M, V, P;
//uniform mat3 normalMatrix;

void main()
{
	vec4 u_camera = vec4(0,0,0,1);
	vec4 vertex = M * vec4(vertexPosition, 1);
	vec3 incident = normalize(vec3(vertex - u_camera));

	// Assume incoming normal is normalized.
	vec3 normal = normalize(mat3(V * M) * vertexNormal);
	
	v_refraction = refract(incident, normal, Eta);
	v_reflection = reflect(incident, normal);
			
	// see http://en.wikipedia.org/wiki/Schlick%27s_approximation
	v_fresnel = R0 + (1.0 - R0) * pow((1.0 - dot(-incident, normal)), 5.0);

	// Output position of the vertex, in clip space : MVP * position
	//gl_Position =  P * V * M * vec4(vertexPosition, 1);
	gl_Position =  P * V * vertex;
	
	// UV of the vertex. No special space for this one.
	UV = vertexUV;
}