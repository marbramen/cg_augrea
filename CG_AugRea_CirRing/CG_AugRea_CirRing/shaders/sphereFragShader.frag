#version 330 core

in vec2 UV;
in vec3 v_refraction;
in vec3 v_reflection;
in float v_fresnel;

// Ouput data
out vec4 color;

// Values that stay constant for the whole mesh.
uniform sampler2D myTexture;

void main()
{
	vec4 refractionColor = texture(myTexture, vec2(normalize(v_refraction)));
	vec4 reflectionColor = texture(myTexture, vec2(normalize(v_reflection)));
	color = vec4(texture( myTexture, UV ).rgb, 0.3);
	color = mix(refractionColor, reflectionColor, v_fresnel);
	color.w = 0.3;
}